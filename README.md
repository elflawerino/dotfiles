# Setup

1. Set ZSH as the default shell
2. Install necessary packages: python3 autojump
3. Install fonts from <https://github.com/romkatv/powerlevel10k#meslo-nerd-font-patched-for-powerlevel10k>
4. Run `zsh install`
5. Restart the shell
